<header class="banner nav-down">
    <nav class="navbar navbar-dark navbar-primary">
        <div class="container-fluid">

            <a class="navbar-brand" href="{{ home_url('/') }}"><img class="svg"
                    src="{{ App\asset_path('images/logo-borgo-divino-light.svg') }}"
                    alt="{{ get_bloginfo('name', 'display') }}"></a>




            <button class="navbar-toggler navbar-toggler-right hamburger hamburger--collapse" type="button"
                data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                aria-expanded="false" aria-label="Toggle navigation">
                <span class="btn-text text-light">Menu</span>
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>

        <div class="collapse container-fluid navbar-collapse" id="navbarSupportedContent">

            @if (has_nav_menu('primary_navigation'))
            {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'navbar-nav offset-md-1
            col-6 col-md-5']) !!}
            @endif


            <div class="offset-md-1 col-6 col-md-5">


                <div class="menu--contact-container">
                    <p class="h2 primary-tit text-light">Contact us</p>
                    <p class="text-light">Via Le Mura, 5 50025 - Montespertoli</p>
                    <p class="text-light">(Firenze) - Italia</p>
                    <p class="text-light"><a
                            href="<?= antispambot('mailto:info@borgodivino.bio'); ?>"><?= esc_html( antispambot('info@borgodivino.bio')); ?></a>
                        <p class="text-light"><a href="tel:+390571671015">+39 0571 671015</a></p>
                </div>


            </div>

        </div>
    </nav>
</header>