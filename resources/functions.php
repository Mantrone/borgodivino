<?php

/**
 * Do not edit anything in this file unless you know what you're doing
 */

use Roots\Sage\Config;
use Roots\Sage\Container;

/**
 * Helper function for prettying up errors
 * @param string $message
 * @param string $subtitle
 * @param string $title
 */
$sage_error = function ($message, $subtitle = '', $title = '') {
    $title = $title ?: __('Sage &rsaquo; Error', 'sage');
    $footer = '<a href="https://roots.io/sage/docs/">roots.io/sage/docs/</a>';
    $message = "<h1>{$title}<br><small>{$subtitle}</small></h1><p>{$message}</p><p>{$footer}</p>";
    wp_die($message, $title);
};

/**
 * Ensure compatible version of PHP is used
 */
if (version_compare('7.1', phpversion(), '>=')) {
    $sage_error(__('You must be using PHP 7.1 or greater.', 'sage'), __('Invalid PHP version', 'sage'));
}

/**
 * Ensure compatible version of WordPress is used
 */
if (version_compare('4.7.0', get_bloginfo('version'), '>=')) {
    $sage_error(__('You must be using WordPress 4.7.0 or greater.', 'sage'), __('Invalid WordPress version', 'sage'));
}

/**
 * Ensure dependencies are loaded
 */
if (!class_exists('Roots\\Sage\\Container')) {
    if (!file_exists($composer = __DIR__.'/../vendor/autoload.php')) {
        $sage_error(
            __('You must run <code>composer install</code> from the Sage directory.', 'sage'),
            __('Autoloader not found.', 'sage')
        );
    }
    require_once $composer;
}

/**
 * Sage required files
 *
 * The mapped array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 */
array_map(function ($file) use ($sage_error) {
    $file = "../app/{$file}.php";
    if (!locate_template($file, true, true)) {
        $sage_error(sprintf(__('Error locating <code>%s</code> for inclusion.', 'sage'), $file), 'File not found');
    }
}, ['helpers', 'setup', 'filters', 'admin', 'walker']);

/**
 * Here's what's happening with these hooks:
 * 1. WordPress initially detects theme in themes/sage/resources
 * 2. Upon activation, we tell WordPress that the theme is actually in themes/sage/resources/views
 * 3. When we call get_template_directory() or get_template_directory_uri(), we point it back to themes/sage/resources
 *
 * We do this so that the Template Hierarchy will look in themes/sage/resources/views for core WordPress themes
 * But functions.php, style.css, and index.php are all still located in themes/sage/resources
 *
 * This is not compatible with the WordPress Customizer theme preview prior to theme activation
 *
 * get_template_directory()   -> /srv/www/example.com/current/web/app/themes/sage/resources
 * get_stylesheet_directory() -> /srv/www/example.com/current/web/app/themes/sage/resources
 * locate_template()
 * ├── STYLESHEETPATH         -> /srv/www/example.com/current/web/app/themes/sage/resources/views
 * └── TEMPLATEPATH           -> /srv/www/example.com/current/web/app/themes/sage/resources
 */
array_map(
    'add_filter',
    ['theme_file_path', 'theme_file_uri', 'parent_theme_file_path', 'parent_theme_file_uri'],
    array_fill(0, 4, 'dirname')
);
Container::getInstance()
    ->bindIf('config', function () {
        return new Config([
            'assets' => require dirname(__DIR__).'/config/assets.php',
            'theme' => require dirname(__DIR__).'/config/theme.php',
            'view' => require dirname(__DIR__).'/config/view.php',
        ]);
    }, true);



// Custom post typer of Activity
function activity_custom_post() {
    // creo e registro il custom post type
    register_post_type( 'activity', /* nome del custom post type */
    // definisco le varie etichette da mostrare nei menù
    array('labels' => array(
    'name' => 'Attività', /* nome, al plurale, dell'etichetta del post type. */
    'singular_name' => 'Attività', /* nome, al singolare, dell'etichetta del post type. */
    'all_items' => 'Tutte le Attività', /* testo nei menu che indica tutti i contenuti del post type */
    'add_new' => 'Aggiungi nuova', /*testo del pulsante Aggiungi. */
    'add_new_item' => 'Aggiungi nuova Attività', /* testo per il pulsante Aggiungi nuovo post type */
    'edit_item' => 'Modifica Attività', /* testo modifica */
    'new_item' => 'Nuovo Attività', /* testo nuovo oggetto */
    'view_item' => 'Visualizza Attività', /* testo per visualizzare */
    'search_items' => 'Cerca Attività', /* testo per la ricerca*/
    'not_found' => 'Nessun Attività trovata', /* testo se non trova nulla */
    'not_found_in_trash' => 'Nessuna Attività trovata nel cestino', /* testo se non trova nulla nel cestino */
    'parent_item_colon' => ''
    ), /* fine dell'array delle etichette del menu */
    'description' => 'Raccolta delle Attività', /* descrizione del post type */
    'public' => true, /* definisce se il post type sia visibile sia da front-end che da back-end */
    'publicly_queryable' => true, /* definisce se possono essere fatte query da front-end */
    'exclude_from_search' => false, /* esclude (false) il post type dai risultati di ricerca */
    'show_ui' => true, /* definisce se deve essere visualizzata l'interfaccia di default nel pannello di amministrazione */
    'query_var' => true,
    'menu_position' => 8, /* definisce l'ordine in cui comparire nel menù di amministrazione a sinistra */
    'menu_icon' => 'dashicons-buddicons-activity', /* imposta l'icona da usare nel menù per il posty type */
    'rewrite' => array( 'slug' => 'Attività', 'with_front' => false ), /* specificare uno slug per leURL */
    'has_archive' => true, /* definisci se abilitare la generazione di un archivio (tipo archive-cd.php) */
    'capability_type' => 'post', /* definisci se si comporterà come un post o come una pagina */
    'hierarchical' => false, /* definisci se potranno essere definiti elementi padri di altri */
    /* la riga successiva definisce quali elementi verranno visualizzati nella schermata di creazione del post */
    'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments',
    'revisions', 'sticky')
    ) /* fine delle opzioni */
    ); /* fine della registrazione */
    
    }
    
    // Inizializzo la funzione
    add_action( 'init', 'activity_custom_post');