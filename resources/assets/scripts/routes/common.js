// add class to whole navbar when menu is open
function menuFullscreen() {
  $('#navbarSupportedContent').on('show.bs.collapse', function () {
    $('nav.navbar').addClass('menu-open');
  })

  $('#navbarSupportedContent').on('hide.bs.collapse', function () {
    $('nav.navbar').removeClass('menu-open');
  })
}

function hamburger() {
  // Look for .hamburger
  var hamburger = document.querySelector('.hamburger');
  // On click
  hamburger.addEventListener('click', function() {
    // Toggle class "is-active"
    hamburger.classList.toggle('is-active');
    // Do something else, like open/close menu
  });
}

export default {
  init() {
    // JavaScript to be fired on all pages
    menuFullscreen();
    hamburger();
    
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
